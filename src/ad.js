const {
  getAdPolicies,
  getAdIntervalInSeconds,
  refreshDatabase
} = require("./db");
const {
  DO_NOT_SHOW_AD_AT_FIRST_REQUEST,
  DISABLE_ALL_ADS,
  PREVENT_ADS_IN_A_ROW
} = require("./constants");

let lastAdShowedAt = Date.now();
let adShowedAtLastRequest = getAdPolicies().includes(
  DO_NOT_SHOW_AD_AT_FIRST_REQUEST
);

function shouldShowAd() {
  refreshDatabase();

  const adIntervalInSeconds = getAdIntervalInSeconds();
  const adPolicies = getAdPolicies();

  if (adPolicies.includes(DISABLE_ALL_ADS)) {
    return false;
  }

  const now = Date.now();
  if (adPolicies.includes(PREVENT_ADS_IN_A_ROW)) {
    if (adShowedAtLastRequest) {
      lastAdShowedAt = now;
      adShowedAtLastRequest = false;
      return false;
    }
  }

  // adInvervalInSeconds초 간격으로 광고 노출
  if (now - lastAdShowedAt >= adIntervalInSeconds * 1000) {
    lastAdShowedAt = now;
    adShowedAtLastRequest = true;
    return true;
  }

  adShowedAtLastRequest = false;
  return false;
}

module.exports = {
  shouldShowAd
};
