const low = require("lowdb");
const FileSync = require("lowdb/adapters/FileSync");

const adapter = new FileSync("db.json");
const db = low(adapter);

function getAdPolicies() {
	return db.get("AD_POLICIES").value();
}

function getAdIntervalInSeconds() {
	return db.get("AD_INTERVAL_IN_SECONDS").value();
}

function refreshDatabase() {
	db.read();
}

module.exports = {
	getAdPolicies,
	getAdIntervalInSeconds,
	refreshDatabase
};
