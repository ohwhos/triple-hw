const express = require("express");
const app = express();
const port = 3000;

const { shouldShowAd } = require("./ad");

const article = {
	title: "TSLA Stock Losses 12% as Tesla Sales Fall in China Despite ...",
	content: "Lorem ipsum dolor sit amet, consectetur adipiscing elit."
};
const ad = {
	title: "광고",
	content: "이 부분에 광고를 넣어주세요"
};

app.get("/article", (req, res) => {
	if (shouldShowAd()) {
		res.json(ad);
	} else {
		res.json(article);
	}
});

app.listen(port, () => console.log(`Listening on port ${port}`));
