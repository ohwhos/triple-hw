# triple-hw

## Key Points

- JSON DB를 통해 실시간으로 관리할 수 있는 광고 정책
- 확장 가능한 시스템. 원하는 정책을 쉽게 추가할 수 있음
- 사용자 경험을 고려한 광고 정책
    - 최초 요청엔 광고가 나오지 않음
    - 광고가 두번 연속으로 나오지 않음

## Start

```bash
npm install
npm start
```